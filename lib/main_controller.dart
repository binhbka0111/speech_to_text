import 'dart:io';
import 'package:app_speech_to_text/toast.dart';
import 'package:battery_info/battery_info_plugin.dart';
import 'package:battery_info/enums/charging_status.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:perfect_volume_control/perfect_volume_control.dart';

class MainController extends GetxController{
  RxDouble currentVol = 0.0.obs;
  @override
  void onInit() async{
    super.onInit();
    PerfectVolumeControl.hideUI = false; //set if system UI is hided or not on volume up/down
    Future.delayed(Duration.zero,() async {
      currentVol.value = await PerfectVolumeControl.getVolume();
    });
    PerfectVolumeControl.stream.listen((volume) {
      currentVol.value = volume;
    });
  }


  decreaseVolume(){
    if(currentVol >= 0.1 && currentVol <=1.0){
      PerfectVolumeControl.setVolume(currentVol.value - 0.1);
    }else{
      PerfectVolumeControl.setVolume(0.0);
    }

    if(currentVol.value == 0.0){
      ToastUtil.show("Âm lượng min");
    }
  }


  increaseVolume(){
    if(currentVol <= 0.9){
      PerfectVolumeControl.setVolume(currentVol.value + 0.1);
    }else{
      PerfectVolumeControl.setVolume(1.0);
    }
    if(currentVol.value == 1.0){
      ToastUtil.show("Âm lượng max");
    }
  }



  getBatteryAndroid()async{
    var infoAndroid =  await BatteryInfoPlugin().androidBatteryInfo;
    var batteryLevel = infoAndroid?.batteryLevel.toString();
    var batteryHealth = infoAndroid?.health.toString();
    var chargingStatus = infoAndroid?.chargingStatus;
    var pluggedStatus = infoAndroid?.pluggedStatus.toString();

    dialogShowInformationBattery(Get.context,batteryLevel,batteryHealth,chargingStatus,pluggedStatus);
  }
  getBatteryIos()async{
    var infoIos = await BatteryInfoPlugin().iosBatteryInfo;
    var batteryLevel = infoIos?.batteryLevel.toString();
    var batteryHealth = "";
    var chargingStatus = infoIos?.chargingStatus;
    var pluggedStatus = "";
    dialogShowInformationBattery(Get.context,batteryLevel,batteryHealth,chargingStatus,pluggedStatus);
  }

  getTextChargingStatus( status){
    switch(status){
      case ChargingStatus.Full:
        return "Sạc đầy";
      case ChargingStatus.Charging:
        return "Đang sạc";
      case ChargingStatus.Discharging:
        return "Không sạc";
      case ChargingStatus.Unknown:
        return "Không sạc";
      default:
        return "";
    }


  }

  dialogShowInformationBattery(context, batteryLevel,batteryHealth,chargingStatus,pluggedStatus) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: Wrap(
            children: [
              Container(
                width: Get.width - 100,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                margin: const EdgeInsets.symmetric(horizontal: 16),
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text("Thông tin pin",style: TextStyle(color: Colors.green,fontSize: 14,decoration: TextDecoration.none),),
                    const SizedBox(height: 8),
                    _richText("% pin hiện tại:", batteryLevel),
                    Visibility(
                      visible: Platform.isAndroid,
                      child: const SizedBox(height: 4),),
                    Visibility(
                        visible: Platform.isAndroid,
                        child: _richText("Tuổi thọ pin:", batteryHealth )),
                    const SizedBox(height: 4),
                    _richText("Tình trạng sạc:", getTextChargingStatus(chargingStatus)),
                    const SizedBox(height: 4),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }


  _richText(title, content) {
    return RichText(
      text: TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: const TextStyle(color: Colors.green,fontSize: 12),
        children: <TextSpan>[
          TextSpan(text: title, style: const TextStyle(color: Colors.black87,fontSize: 12)),
          // ignore: prefer_interpolation_to_compose_strings
          TextSpan(text: ' ' + content),
        ],
      ),
    );
  }


}

